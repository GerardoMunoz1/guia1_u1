# Ejercicio 1 - Guia 1
Gerardo Muñoz
### Instalación

Para ejecutar el programa, se debe tener todos los archivos presentes en el repositorio y escribir en una terminal:

```
make
```

Con "make", asegura su correcta compilación, generando el archivo "app1". Luego escribir:

```
./sistema
```

Actualmente, solamente guarda los datos y los imprime por pantalla.


