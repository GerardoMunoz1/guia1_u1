#include <iostream>
#include "Sistema.h"

using namespace std;

Sistema::Sistema(){
}

// setters y getters de los datos
void Sistema::set_nombre(string nombre_profesor){
    this->nombre_profesor = nombre_profesor;
}

void Sistema::set_sexo(string sexo_profesor){
    this->sexo_profesor = sexo_profesor;
}

void Sistema::set_edad(int edad_profesor){
    this->edad_profesor = edad_profesor;
}

string Sistema::get_nombre(){
    return this->nombre_profesor;
}

string Sistema::get_sexoProfesor(){
    return this->sexo_profesor;
}

int Sistema::get_edad(){
    return this->edad_profesor;
}
