#include <iostream>
#include "Sistema.h"

using namespace std;

void ingresar_datos(Sistema *profesores, int max_prof){

    int flag = 0;

    string nombre_profesor;
    string sexo_profesor;
    int edad_profesor;

    for (int i=0; i<max_prof; i++){

        cout << "Nombre: ";
        cin >> nombre_profesor;

        cout << "Sexo (M=Masculino / F=Femenino): ";
        cin >> sexo_profesor;

        while (flag != 1){
            if (sexo_profesor != "F" && sexo_profesor != "f" && sexo_profesor != "M" && sexo_profesor != "m"){
                cout << "Opción ingresada no válida. Utilizar (M/F): ";
                cin >> sexo_profesor;
                flag = 0;
            }
            else{
                flag = 1;
            }
        }

        cout << "\nEdad: ";
        cin >> edad_profesor;

        profesores[i].set_nombre(nombre_profesor);

        if (sexo_profesor == "M" || sexo_profesor == "m"){
            profesores[i].set_sexo("Masculino");
        }
        else if (sexo_profesor == "F" || sexo_profesor == "f"){
            profesores[i].set_sexo("Femenino");
        }
        else{
            cout << "Error.";
        }
        profesores[i].set_edad(edad_profesor);

    }
}

void imprimir_datos_profesores(Sistema *profesores, int max_prof){

    system("clear");

    for (int i=0; i<max_prof; i++){

        cout << "\nNOMBRE: " << profesores[i].get_nombre() << endl;
        cout << "SEXO: " << profesores[i].get_sexoProfesor() << endl;
        cout << "EDAD: " << profesores[i].get_edad() << endl;
    }
}

void profesor_mas_viejo(Sistema *profesores, int max_prof){
    //edad_profesor = edad_profesor;
    int contador_mayores = 0;
    int profesor_cero = 0; //edad para partir la comapracion
    int profesor_mayor;

    for (int i=0; i < max_prof; i++){

        if (profesor_cero >= profesores[i].get_edad()){
            profesor_mayor = profesores[i].get_edad();
        }
        else if(profesor_cero < profesores[i].get_edad()){
            profesor_mayor = profesor_mayor;
        }
        else{
            cout << "Error.";
        }
        profesor_cero = profesores[i].get_edad();
    }


}

int main(){

    string max_prof;
    cout << "       [Sistema de información de profesores]" << endl;
    cout << "\n¿Cuántos profesores desea ingresar al sistema?: ";
    getline(cin, max_prof);

    Sistema profesores[stoi(max_prof)];

    ingresar_datos(profesores, stoi(max_prof));
    imprimir_datos_profesores(profesores, stoi(max_prof));
    profesor_mas_viejo(profesores, stoi(max_prof));

    return 0;
}
