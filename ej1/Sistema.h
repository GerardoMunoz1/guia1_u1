#include <iostream>

using namespace std;

#ifndef SISTEMA_H
#define SISTEMA_H

class Sistema{

    private:
        string nombre_profesor;
        string sexo_profesor;
        int edad_profesor;
        int max_prof;
        int promedio;
        int suma_total;
        int contador_mayores;
        int profesor_cero;
        int profesor_mas_viejo;

    public:
        Sistema();
        void set_nombre(string nombre_profesor);
        void set_sexo(string sexo_profesor);
        void set_edad(int edad_profesor);
        string get_nombre();
        string get_sexoProfesor();
        int get_edad();
};
#endif
