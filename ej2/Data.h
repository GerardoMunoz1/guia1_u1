#include <iostream>

using namespace std;

#ifndef DATA_H
#define DATA_H

class Data{

    private:
        string nombre;
        string sexo;
        int edad;
        int x;
        string domicilio;
        int numero_telefonico;
        string seguro_medico;
        int cantidad_ninos;
        int cantidad_jovenes;
        int cantidad_adultos;
        int porcentaje_ninos;
        int porcentaje_jovenes;
        int porcentaje_adultos;


    public:
        Data();
        void set_nombre(string nombre);
        void set_sexo(string sexo);
        void set_edad(int edad);
        void set_domicilio(string domicilio);
        void set_telefono(int numero_telefonico);
        void set_seguroMedico(string seguro_medico);

        string get_nombre();
        string get_sexo();
        int get_edad();
        string get_domicilio();
        int get_numeroTelefono();
        string get_seguroMedico();

};
#endif
