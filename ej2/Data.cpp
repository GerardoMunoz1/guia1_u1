#include <iostream>
#include "Data.h"

using namespace std;

Data::Data(){
}

void Data::set_nombre(string nombre){
    this->nombre = nombre;
}

void Data::set_sexo(string sexo){
    this->sexo = sexo;
}

void Data::set_edad(int edad){
    this->edad = edad;
}

void Data::set_domicilio(string domicilio){
    this->domicilio = domicilio;
}

void Data::set_telefono(int numero_telefonico){
    this->numero_telefonico = numero_telefonico;
}

void Data::set_seguroMedico(string seguro_medico){
    this->seguro_medico = seguro_medico;
}

string Data::get_nombre(){
    return this->nombre;
}

string Data::get_sexo(){
    return this->sexo;
}

int Data::get_edad(){
    return this->edad;
}

string Data::get_domicilio(){
    return this->domicilio;
}

int Data::get_numeroTelefono(){
    return this->numero_telefonico;
}

string Data::get_seguroMedico(){
    return this->seguro_medico;
}
