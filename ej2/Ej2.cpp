#include <iostream>
#include <stdlib.h>
#include "Data.h"

using namespace std;

void ingresar_datos(Data *sistema, int x){

    string nombre;
    int edad;
    string sexo;
    string domicilio;
    int numero_telefonico;
    string seguro_medico;

    for (int i=0; i<x; i++){

        cout << "\nPACIENTE n°" << i+1 << endl;
        cout << "NOMBRE: ";
        cin >> nombre;
        cout << "EDAD: ";
        cin >> edad;
        cout << "SEXO (M/F): ";
        cin >> sexo;

        while (sexo != "M" && sexo != "m" && sexo != "F" && sexo != "f"){
            cout << "POR FAVOR 'M' O 'F': ";
            cin >> sexo;
        }
        cout << "DOMICILIO: ";
        cin >> domicilio;
        cout << "NUMERO DE TELEFONO (+56 9): ";
        cin >> numero_telefonico;
        cout << "¿SEGURO MEDICO? (S/N): ";
        cin >> seguro_medico;

        while (seguro_medico != "s" && seguro_medico != "S" && seguro_medico != "n" && seguro_medico != "N"){
            cout << "POR FAVOR 'S' O 'N': ";
            cin >> seguro_medico;
        }

        sistema[i].set_nombre(nombre);
        sistema[i].set_edad(edad);

        if (sexo == "M" || sexo == "m"){
            sistema[i].set_sexo("MASCULINO");
        }
        else if (sexo == "F" || sexo == "f"){
            sistema[i].set_sexo("FEMENINO");
        }

        sistema[i].set_domicilio(domicilio);
        sistema[i].set_telefono(numero_telefonico);

        if (seguro_medico == "S" || seguro_medico == "s"){
            sistema[i].set_seguroMedico("SI");
        }
        else if (seguro_medico == "N" || seguro_medico == "n"){
            sistema[i].set_seguroMedico("NO");
        }
    }

    cout << "\n> DATOS GUARDADOS CON ÉXITO" << endl;

}

void calcular_porcentajes(Data *sistema, int x){

    int contador_hombres = 0;
    int contador_mujeres = 0;
    int porcentaje_hombres = 0;
    int porcentaje_mujeres = 0;
    int cantidad_ninos = 0;
    int cantidad_jovenes = 0;
    int cantidad_adultos = 0;
    int porcentaje_ninos = 0;
    int porcentaje_jovenes = 0;
    int porcentaje_adultos = 0;
    int personas_conSeguro = 0;
    int porcentaje_seguro = 0;

    for (int i=0; i<x; i++){
        // CALCULO PORCENTAJE DE EDADES
        if (sistema[i].get_edad() <= 13){
            cantidad_ninos++;
        }
        else if (sistema[i].get_edad() > 13 && sistema[i].get_edad() < 30){
            cantidad_jovenes++;
        }
        else if (sistema[i].get_edad() >= 30){
            cantidad_adultos++;
        }
        else{
            cout << "Error.";
        }
        // CALCULO PORCENTAJE DE HOMBRES/MUJERES
        if (sistema[i].get_sexo() == "MASCULINO"){
            contador_hombres++;
        }
        else if (sistema[i].get_sexo() == "FEMENINO"){
            contador_mujeres++;
        }
        else{
            cout << "Error.";
        }
        // CALCULO PORCENTAJE DE QUIENES POSEEN SEGURO MEDICO
        if (sistema[i].get_seguroMedico() == "SI"){
            personas_conSeguro++;
        }
    }
    ////////////////////////////////////////////////////////////////////
    porcentaje_ninos = (cantidad_ninos * 100) / x;
    porcentaje_jovenes = (cantidad_jovenes * 100) / x;
    porcentaje_adultos = (cantidad_adultos * 100) / x;
    ////////////////////////////////////////////////////////////////////
    porcentaje_hombres = (contador_hombres * 100) / x;
    porcentaje_mujeres = (contador_mujeres * 100) / x;
    ////////////////////////////////////////////////////////////////////
    porcentaje_seguro = (personas_conSeguro * 100) /x;

    cout << "\n> PORCENTAJE DE NIÑOS: " << porcentaje_ninos << "%" << endl;
    cout << "> PORCENTAJE DE JOVENES: " << porcentaje_jovenes << "%" << endl;
    cout << "> PORCENTAJE DE ADULTOS: " << porcentaje_adultos << "%" << endl;
    ////////////////////////////////////////////////////////////////////
    cout << "\n> PORCENTAJE DE HOMBRES: " << porcentaje_hombres << "%" << endl;
    cout << "> PORCENTAJE DE MUJERES: " << porcentaje_mujeres << "%" << endl;
    ////////////////////////////////////////////////////////////////////
    cout << "\n> PORCENTAJE DE PERSONAS CON SEGURO MEDICO: " << porcentaje_seguro << "%" << endl;

}

void imprimir_datos_pacientes(Data *sistema, int x){

    system("clear");
    cout << "\n     [DATOS DE PACIENTES INGRESADOS]" << endl;

    for (int i=0; i<x; i++){

        cout << "\nPACIENTE " << i+1 << endl;
        cout << "NOMBRE: " << sistema[i].get_nombre() << endl;
        cout << "SEXO: " << sistema[i].get_sexo() << endl;
        cout << "EDAD: " << sistema[i].get_edad() << endl;
        cout << "DOMICILIO: " << sistema[i].get_domicilio() << endl;
        cout << "NUMERO TELEFONICO: +569 " << sistema[i].get_numeroTelefono() << endl;
        cout << "SEGURO MEDICO: " << sistema[i].get_seguroMedico() << endl;
    }

    cout << "\n> TOTAL DE PACIENTES: " << x << endl;

//    cout << "hola" << sistema.get_porcentaje() << endl;
}

void buscar_datos_paciente(Data *sistema, int x){

    string nombre_buscar;

    cout << "\n     [BUSCAR PACIENTE]";
    cout << "\n> NOMBRE: ";
    cin >> nombre_buscar;

    for (int i=0; i<x; i++){

        if (sistema[i].get_nombre() == nombre_buscar){

            cout << "\nSE HA ENCONTRADO EL PACIENTE: " << nombre_buscar << endl;
            cout << "\nNOMBRE: " << sistema[i].get_nombre() << endl;
            cout << "SEXO: " << sistema[i].get_sexo() << endl;
            cout << "EDAD: " << sistema[i].get_edad() << endl;
            cout << "DOMICILIO: " << sistema[i].get_domicilio() << endl;
            cout << "NUMERO TELEFONICO: +569 " << sistema[i].get_numeroTelefono() << endl;
            cout << "SEGURO MEDICO: " << sistema[i].get_seguroMedico() << endl;
        }
        else{
            cout << "> NO SE ENCONTRÓ NINGÚN PACIENTE CON EL NOMBRE: " << nombre_buscar << endl;
            cout << "\nBUSCANDO... ";
        }
    }
}

int main(){

    string x;

    cout << "¿Cuántos pacientes desea ingresar al sistema?: ";
    getline(cin, x);

    Data sistema[stoi(x)];

    ingresar_datos(sistema, stoi(x));
    imprimir_datos_pacientes(sistema, stoi(x));
    calcular_porcentajes(sistema, stoi(x));
    buscar_datos_paciente(sistema, stoi(x));

    return 0;
}
